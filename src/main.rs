use axum::{routing::get, Router};
use clap::Parser;
use lazy_static::lazy_static;
use linux_embedded_hal::{Delay, I2cdev};
use prometheus::{self, register_gauge, Encoder, Gauge, TextEncoder};
use scd4x::Scd4x;
use std::time::Duration;

lazy_static! {
    static ref TEMPERATURE: Gauge = register_gauge!("scd4x_temperature", "Temperature").unwrap();
    static ref HUMIDITY: Gauge = register_gauge!("scd4x_humidity", "Humidity").unwrap();
    static ref CO2: Gauge = register_gauge!("scd4x_co2", "CO2 concentration").unwrap();
}

#[derive(Parser, Debug)]
struct Args {
    #[clap(value_parser)]
    device: String,

    #[clap(short, long, default_value = "0.0.0.0:9009")]
    bind: String,
}

async fn metrics() -> Vec<u8> {
    let mut result = Vec::new();
    let metrics = prometheus::gather();
    TextEncoder::new().encode(&metrics, &mut result).unwrap();
    result
}

#[tokio::main]
async fn main() {
    tracing_subscriber::fmt::init();

    let args = Args::parse();

    let mut device = Scd4x::new(
        I2cdev::new(args.device).expect("Failed to open device!"),
        Delay,
    );

    device.wake_up();
    device.stop_periodic_measurement().unwrap();
    device.reinit().unwrap();
    device.set_automatic_self_calibration(true).unwrap();
    device.start_periodic_measurement().unwrap();

    let mut timer = tokio::time::interval(Duration::from_secs(5));

    tokio::spawn(async move {
        loop {
            timer.tick().await;

            let result = device.measurement();
            match result {
                Ok(data) => {
                    TEMPERATURE.set(data.temperature.into());
                    HUMIDITY.set(data.humidity.into());
                    CO2.set(data.co2.into());
                }
                Err(e) => {
                    tracing::warn!("Failed to read data: {:?}", e);
                }
            };
        }
    });

    let app = Router::new().route("/metrics", get(metrics));

    let addr = args.bind.parse().unwrap();
    tracing::debug!("listening on {}", addr);
    axum_server::bind(addr)
        .serve(app.into_make_service())
        .await
        .unwrap();
}
