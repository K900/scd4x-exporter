self:
{
  config,
  lib,
  pkgs,
  ...
}:
with lib;
let
  cfg = config.services.scd4x-exporter;
  pkgDesc = "Sensirion SCD4X CO2 sensor Prometheus exporter";
  pkg = self.packages.${pkgs.stdenv.hostPlatform.system}.scd4x-exporter;
in
{
  options = {
    services.scd4x-exporter = {
      enable = mkEnableOption pkgDesc;

      i2cDevice = mkOption {
        type = types.str;
        description = "i2c device node";
      };

      listenAddress = mkOption {
        type = types.str;
        description = "listen address";
      };
    };
  };

  config = mkIf cfg.enable {
    systemd.services.scd4x-exporter = {
      description = pkgDesc;
      wantedBy = [ "multi-user.target" ];
      after = [ "network-online.target" ];
      wants = [ "network-online.target" ];
      serviceConfig = {
        Restart = "always";
        ExecStart = "${pkg}/bin/scd4x-exporter ${cfg.i2cDevice} --bind=${cfg.listenAddress}";
      };
    };
  };
}
